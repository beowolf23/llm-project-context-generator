import os
import sys

def get_command_arguments():

    # Get the command line arguments
    args = sys.argv

    # Check if the command line arguments are valid
    if len(args) != 3:
        print("Usage: python gptcontext.py <base_directory> <file_extension>")
        sys.exit(1)

    # Check if the input file exists
    if not os.path.exists(args[1]):
        print("Error: Input file not found")
        sys.exit(1)

    if args[1] == "."  or args[1] == "./":
        args[1] = os.getcwd()

    return args[1], args[2]

def get_files(base_directory, file_extension):

    # Get all the files in the base directory
    files = []
    for root, directories, filenames in os.walk(base_directory):
        for filename in filenames:
            if filename.endswith(file_extension):
                files.append(os.path.join(root, filename))

    return files


def get_files_output(files):

    # Get the output for the files
    output = ""
    for file in files:
        with open(file) as f:
            s = f.read()
            if len(s) == 0:
                continue

        output = add_separator(output)
        output += "# Filename: " +  file + 2 * "\n" + s

    return output

def add_separator(output):

    # Add a separator to the output
    output += 2 * "\n" + "=" * 80 + 2 * "\n"

    return output

def main():

    # Get the command line arguments
    base_directory, file_extension = get_command_arguments()

    # Get the files in the base directory
    files = get_files(base_directory, file_extension)

    # Get the output for the files
    output = get_files_output(files)

    # Print the output
    print(output)

if __name__ == "__main__":
    main()